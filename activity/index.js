console.log("Hello World");

let firstName = "Carl";
let lastName = "Villarta";
let age = "20";
let hobbies = ["Coding", "Designing", "Biking"];
let workAddress = {
  houseNumber: "123",
  street: "Pagsabungan Street",
  city: "Mandaue City",
  state: "Cebu",
};

console.log("First Name: " + firstName);
console.log("Last Name: " + lastName);
console.log("Age: " + age);

console.log("Hobbies: ");
console.log(hobbies);

console.log("Work Address: ");
console.log(workAddress);

let fullName = "Steve Rogers";
console.log("My full name is: " + fullName);

let currentAge = 40;
console.log("My current age is: " + currentAge);

let friends = ["Tony", "Bruce", "Thor", "Natasha", "Clint", "Nick"];
console.log("My Friends are: ");
console.log(friends);

let profile = {
  username: "captain_america",
  fullName: "Steve Rogers",
  age: 40,
  isActive: false,
};
console.log("My Full Profile: ");
console.log(profile);

fullName = "Bucky Barnes";
console.log("My bestfriend is: " + fullName);

let lastLocation = "Arctic Ocean";
lastLocation = "Atlantic Ocean";
console.log("I was found frozen in: " + lastLocation);
